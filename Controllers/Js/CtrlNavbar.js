var Nav = new Vue({
    el: "#Nav",
    data: {

    },
    methods: {
        CerrarSession: function(){
            let peticion = new FormData();
            peticion.append("destroySession" , true);
            axios.post("Controllers/PHP/CtrlLogin.php", peticion)
            .then(function(response){
                console.log(response);
                if(response.data.session == "destroy"){
                    location.reload();
                }
            })
        }
    },
})