var Reg = new Vue({
    el : "#registro",
    data:{
        // datos de registro: 
        nombre: "",
        apellidos: "",
        correo: "",
        nick: "", // opcional
        pass : "",

        //alertas 
        alert: false,

        input_nombre_border: "",
        input_apellidos_border: "",
        input_email_border: "",
        input_nick_border: "",
        input_pass_border: "",

        // estados del sistema:
        procesando : false,
    },
    methods: {
        RegistroNuevoUser: function (){
            
            // variables para iniciar el proceso: 
            this.input_nombre_border = "";
            this.input_apellidos_border = "";
            this.input_email_border = "";
            this.input_pass_border = "";
            this.input_nick_border = "",
            this.procesando = true;
            this.alert = false;

            let formdata = new FormData();
            formdata.append("peticionAccesoRegistro" , true);
            formdata.append("nombre" , this.nombre);
            formdata.append("apellidos" , this.apellidos);
            formdata.append("correo" , this.correo);
            formdata.append("pass" , this.pass);
            formdata.append("nick" , this.nick);

            axios.post("Controllers/PHP/CtrlLogin.php" , formdata)
            .then(function(response){
                let obj = response.data;
                console.log(response);
                if(obj.respuesta == "preg_math_nombre"){
                    Reg.alert="Nombre no disponible, incluya caracteres [a-zA-Z]";
                    Reg.input_nombre_border = "border-danger";
                    Reg.procesando = false;
                }else if(obj.respuesta == "preg_math_apellidos"){
                    Reg.alert="Apellidos incorrectos, incluya caracteres [a-zA-Z]";
                    Reg.input_apellidos_border = "border-danger";
                    Reg.procesando = false;
                }else if(obj.respuesta == "correo_incorrecto"){
                    Reg.alert="Correo incorrecto  (ej: minombre@mail.com)";
                    Reg.input_email_border = "border-danger";
                    Reg.procesando = false;
                }else if(obj.respuesta == "correo_existente"){
                    Reg.alert="Ya tiene una cuenta con la dirección de correo electrónico ingresada";
                    Reg.input_email_border = "border-danger";
                    Reg.procesando = false;
                }else if(obj.respuesta == "preg_math_nick"){
                    Reg.alert="Usuario incorrecto, incluya caracteres [a-zA-Z0-9]";
                    Reg.input_nick_border = "border-danger";
                    Reg.procesando = false;
                }else if(obj.respuesta == "nick_existente"){
                    Reg.alert="EL usuario ingresado no está disponible";
                    Reg.input_nick_border = "border-danger";
                    Reg.procesando = false;
                }else if(obj.respuesta == "campos_vacios"){
                    if(obj.campos_vacios["nombre"]){Reg.input_nombre_border = "border-danger"}
                    if(obj.campos_vacios["apellidos"]){Reg.input_apellidos_border = "border-danger"}
                    if(obj.campos_vacios["correo"]){Reg.input_email_border = "border-danger"}
                    if(obj.campos_vacios["pass"]){Reg.input_pass_border = "border-danger"}
                    Reg.procesando = false;
                }else if (obj.respuesta == "OK"){
                    location.reload();
                }
            })
            
        }
    },
})