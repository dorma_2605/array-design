
  
<div class="container-fluid page-body-wrapper full-page-wrapper" id="login">
  <div class="content-wrapper d-flex align-items-center auth">
    <div class="row flex-grow">
      <div class="col-lg-4 mx-auto">
        <div class="auth-form-light text-left p-5">
          <div class="brand-logo">
            <img src="assets/images/LOGO_ARRAY_DESIGN.png">
          </div>
          <h4>¡Hola! empecemos</h4>
          <div v-show="alert" class="alert alert-warning" role="alert">
              <P v-text="alert"></P>
          </div>

          <form class="pt-3">
             
            <div class="form-group" @keyup.enter="Login()">
              <input v-model="user" type="text" class="form-control form-control-sm " 
              id="exampleInputEmail1" placeholder="Usuario o correo electrónico">
            </div>
            <div class="form-group">
              <input v-model="pass" type="password" class="form-control form-control-sm" 
              id="exampleInputPassword1" placeholder="Contraseña">
            </div>
            
            <div class="mt-3">
              <button type="button" @click="Login()" v-show="!procesando"
                class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn"
               href="#">ACCEDER</button>
               <button  type="button" v-show="procesando"
                class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn">
                Cargando...</button>
            </div>
            <div class="my-2 d-flex justify-content-between align-items-center">
              <div class="form-check">
                <label class="form-check-label text-muted">
                  <input type="checkbox" class="form-check-input"> Recuerdame</label>
              </div>
              <a href="#" class="auth-link text-black">Olvidaste tu contraseña?</a>
            </div>
            <!-- <div class="mb-2">
              <button type="button" class="btn btn-block btn-facebook auth-form-btn">
                <i class="mdi mdi-facebook mr-2"></i>Connect using facebook </button>
            </div> -->
            <div class="text-center mt-4 font-weight-light"> ¿Aún no tienes una cuenta? <a href="registro" class="text-primary">Crear cuenta</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- content-wrapper ends -->
</div>


<script src = "Controllers/Js/CtrlLogin.js"></script>
     