
      <div class="container-fluid page-body-wrapper full-page-wrapper" id="registro">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-5  mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="brand-logo">
                  <img src="assets/images/LOGO_ARRAY_DESIGN.png">
                </div>
                <h4>Nuevo en #ArrayDesign?</h4>
                <h6 class="font-weight-light">Crea tu cuenta</h6>

                <div v-show="alert" class="alert alert-warning" role="alert">
                    <P v-text="alert"></P>
                </div>
                <form class="pt-3">
                  <div class="form-group">
                    <input type="text" :class="['form-control form-control-sm',input_nombre_border]" 
                    v-model="nombre" placeholder="Nombre">
                  </div>
                  <div class="form-group">
                    <input type="text" :class="['form-control form-control-sm' , input_apellidos_border]" 
                    v-model="apellidos" placeholder="Apellidos">
                  </div>
                  <div class="form-group">
                    <input type="email" :class="['form-control form-control-sm',input_email_border ]" 
                    v-model="correo" placeholder="Correo electrónico">
                  </div>
                  <div class="form-group">
                    <input type="text" :class="['form-control form-control-sm' , input_nick_border]" 
                    v-model="nick" placeholder="Nick o usuario (opcional)">
                  </div>
                 
                  <div class="form-group">
                    <input type="password" :class="['form-control form-control-sm' , input_pass_border]" 
                    v-model="pass" placeholder="Contraseña" >
                  </div>
                  <!-- <div class="mb-4">
                    <div class="form-check">
                      <label class="form-check-label text-muted">
                        <input type="checkbox" class="form-check-input"> I agree to all Terms & Conditions </label>
                    </div>
                  </div> -->
                  <div class="mt-3">
                    <button v-show="!procesando" @click="RegistroNuevoUser()" type="button"
                    class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" >Regístrate</button>

                    <button v-show="procesando" type="button"
                    class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" >Cargando...</button>
                  </div>
                  <div class="text-center mt-4 font-weight-light"> Ya tienes una cuenta? <a href="./" class="text-primary">Acceder</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
   

   <script src="Controllers/Js/CtrlRegistro.js"> </script>