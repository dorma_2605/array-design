<?php
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Inicio | #ARRAYDESIGN</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />

    <script src="assets/vue.js"></script>
    <script src="assets/axios.min.js"></script>

  </head>
  <body>





    <div class="container-scroller">
      <?php 
            if (isset($_SESSION["UserLoggedIn"]) && @$_SESSION["UserLoggedIn"]["estado"] == "ok") {
              $_ROL_USER_ = $_SESSION["UserLoggedIn"]["user"]["rol"];
                include"views/static/navbar.php";
                ?><!-- partial -->
                <div class="container-fluid page-body-wrapper">
                  <?php include"views/static/sidebar.php" ?>
                  <!-- partial -->
                  <div class="main-panel">
                    <div class="content-wrapper">
                    <?php 
                      if (!isset($_GET["_RuTa_"])) 
                      {
                        if ($_ROL_USER_ == "cliente") {
                          include"views/modulos/dashboard/dashboard.client.php";
                        }else{
                          include"views/modulos/dashboard/dashboard.admin.php";
                        }
                      }else{
                        include"views/modulos/".$_GET['_RuTa_']."/".$_GET['_RuTa_'].".".$_ROL_USER_.".php";
                      }
                    ?>
                    </div>
                    <!-- content-wrapper ends -->
                    <?php include"views/static/footer.php" ?>
                    <!-- partial -->
                  </div>
                  <!-- main-panel ends -->
                </div> <?php
            }// si no existe la sesion del usuario y además no es estado "OK"
            // requerimos al LOGIN
            else{
                if(@$_GET["_RuTa_"] == "registro"){
                    require_once"views/access/signout/registro.php";
                }else{
                    require_once"views/access/login/login.php";
                }
                
            }
      ?>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->










    <!-- plugins:js -->
    <script src="assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="assets/vendors/chart.js/Chart.min.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="assets/js/off-canvas.js"></script>
    <script src="assets/js/hoverable-collapse.js"></script>
    <script src="assets/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="assets/js/dashboard.js"></script>
    <script src="assets/js/todolist.js"></script>
    <!-- End custom js for this page -->
  </body>

</html>