<?php
require_once"conexion.php";
class MdlUsuarios{

    static public function RegistroSimpleClientes($nombre, $apellidos, $correo, $nickname, $pass, $fecha_registro)
    {
        $stmt = Conection::conectar()->prepare("INSERT INTO 
        ad_usuarios (nombre , apellidos , nick , correo , contrasena , rol , fecha_registro ) 
        VALUES (:nombre , :apellidos , :nick , :correo , :contrasena , :rol , :fecha_registro) ");
        $rol = "cliente";
        $stmt->bindParam(":nombre", $nombre, PDO::PARAM_STR);
        $stmt->bindParam(":apellidos", $apellidos, PDO::PARAM_STR);
        $stmt->bindParam(":nick", $nickname, PDO::PARAM_STR);
        $stmt->bindParam(":correo", $correo, PDO::PARAM_STR);
        $stmt->bindParam(":contrasena", $pass, PDO::PARAM_STR);
        $stmt->bindParam(":rol", $rol, PDO::PARAM_STR);
        $stmt->bindParam(":fecha_registro", $fecha_registro, PDO::PARAM_STR);
        

        if($stmt->execute()){
          return true;
        }else{
          return false;
        }
        $stmt->close();   
    }   

}